import time
import random
import sys
from directkeys import PressKey, ReleaseKey,W, A, S, D
from keys import Keys
import pyautogui

random_nr = 1
selected_key = None
nr_purchases = 0
at_confirm_flag = False

def walk_around():
    while(True):
        random_nr = random.randint(0,100)
        if random_nr % 3 == 0:
            print('Selected W')
            selected_key = W
        elif random_nr % 5 == 0:
            print('Selected A')
            selected_key = A
        elif random_nr % 2 == 0:
            print('Selected D')
            selected_key = D
        else:
            print('Selected S')
            selected_key = S
        print('Pressing key')
        PressKey(selected_key)
        time.sleep(random.randint(2,10))
        print('Released key')
        ReleaseKey(selected_key)
        time_sleep = random.randint(1,10)
        print('Sleeping for {} seconds'.format(time_sleep))
        time.sleep(time_sleep)

def lb_click(keys):
    keys.directMouse(buttons=keys.mouse_lb_press)
    time.sleep(0.5)
    keys.directMouse(buttons=keys.mouse_lb_release)

def rb_click(keys):
    keys.directMouse(buttons=keys.mouse_rb_press)
    time.sleep(0.5)
    keys.directMouse(buttons=keys.mouse_rb_release)

def exit_computer(keys):
    for i in range(0,3):
        time.sleep(0.5)
        rb_click(keys)

def use_computer_resupply():
    global nr_purchases
    global at_confirm_flag
    print("Using computer")
    time_sleep = random.randint(1,10)
    print('Sleeping for {} seconds'.format(time_sleep))
    time.sleep(time_sleep)
    keys = Keys()

    # mouse movement
    print("Enter logistics")
    #enter Disruption
    start = pyautogui.locateOnScreen('img/enter.png',confidence=.9)#If the file is not a png file it will not work
    print(start)
    pyautogui.moveTo(start)#Moves the mouse to the coordinates of the image
    lb_click(keys)
    time_sleep = random.randint(1,10)
    print('Entered, Sleeping for {} seconds'.format(time_sleep))
    time.sleep(time_sleep)
    #check if supplies level is empty
    try:
        #pyautogui.locateOnScreen('supplies.png')
        print("Supplies are being consumed")
        try:
            ressuply = pyautogui.locateOnScreen("img/ressuply.png",confidence=.9)
            ressuply = pyautogui.locateOnScreen("img/ressuply.png",confidence=.9)
            ressuply = pyautogui.locateOnScreen("img/ressuply.png",confidence=.9)

            pyautogui.moveTo(ressuply)#Moves the mouse to the coordinates of the image\
            pyautogui.moveTo(ressuply)#Moves the mouse to the coordinates of the image
            pyautogui.moveTo(ressuply)#Moves the mouse to the coordinates of the image

            lb_click(keys)
            print("Resupply clicked, box -> " + str(ressuply))
            ressuply = None
            time.sleep(2)
            try:
                ressuply = pyautogui.locateOnScreen("img/buy75k.png",confidence=.9)
                ressuply = pyautogui.locateOnScreen("img/buy75k.png",confidence=.9)
                ressuply = pyautogui.locateOnScreen("img/buy75k.png",confidence=.9)
                pyautogui.moveTo(ressuply)#Moves the mouse to the coordinates of the image
                pyautogui.moveTo(ressuply)#Moves the mouse to the coordinates of the image
                pyautogui.moveTo(ressuply)#Moves the mouse to the coordinates of the image
                lb_click(keys)
                print("Buy supplies clicked, box -> " +  str(ressuply))
                if ressuply is None:
                    print("(!) Supplies not consumed yet, try later")
                else:
                    ressuply = None
                    time.sleep(2)
                    try:
                        if nr_purchases < 50:
                            print("75k purchase available!")
                            ressuply = pyautogui.locateOnScreen("img/confirm.png",confidence=.9)
                            time.sleep(3)
                            if at_confirm_flag == False and ressuply is not None:
                                print("(!) Confirm flag is False (supplies might be left to consume), turning to True, relaunch..")
                                at_confirm_flag = True
                            elif ressuply is not None:
                                print("(!) Confirm flag is True, purchasing items afte 500 seconds")
                                time.sleep(500)
                                at_confirm_flag = False
                                #ressuply = pyautogui.locateOnScreen("confirm.png",confidence=.9)
                                if ressuply is None:
                                    print("(!!!!!!!!!!!!!!!!!!!!!!!!) Purchase already madeeeeeeeeeeeeeeeeeeeeeeeeeee!")
                                else:
                                    print("Confirm clicked, box -> " +  str(ressuply))
                                    pyautogui.moveTo(ressuply)#Moves the mouse to the coordinates of the image
                                    lb_click(keys)
                                    ressuply = None
                                    print("Purchase completed")
                                    nr_purchases += 1
                            else:
                                print("(=======================================) Purchase already madeeeeeeeeeeeeeeeeeeeeeeeeeee ! =======================================")
                        else:
                            print("More than 2 purchases made, exiting program. nr_purchases = " + str(nr_purchases))
                            exit()

                    except:
                        print("error at confirm buying, not enough resources have been consumed yet")
            except:
                print("error at buy supplies")
        except:
            print("error at resupply")
    except:
        print("There are supplies left")
    exit_computer(keys)
    time.sleep(2)
    exit_computer(keys)

def enter_computer():
    #press E
    time.sleep(3)
    keys = Keys()
    keys.directKey("e")
    time.sleep(0.5)
    keys.directKey("e", keys.key_release)

    time.sleep(5)
    keys = Keys()
    keys.directKey("RETURN")
    time.sleep(0.5)
    keys.directKey("RETURN", keys.key_release)
    #wait 5 seconds
    #press enter
    #eait 5 seconds

def desk_operation():
    while True:
        time.sleep(6)
        print("Enter computer")
        enter_computer()
        time.sleep(6)
        print("Use computer")
        use_computer_resupply()
        print("Sleeping for 500 secons now")
        time.sleep(500)

desk_operation()